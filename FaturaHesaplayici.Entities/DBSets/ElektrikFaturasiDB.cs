﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.Entities.DBSets
{
    [Table("ElektrikFaturasi")]
    public class ElektrikFaturasiDB : FaturaBaseDB
    {
        public ElektrikFaturasiDB()
        {
        }

        public double TekZamanBirimFiyat { get; set; }
        public double DagitimBedeliBirimFiyat { get; set; }

    }
}
