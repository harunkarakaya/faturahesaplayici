﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.DataAccessLayer.EntityFramework
{
    public class Singleton
    {
        protected static DatabaseContext context { get; set; }
        protected static object _lock = new object();

        protected Singleton()
        {
            CreateContext();
        }

        private static void CreateContext()
        {
            if(context == null)
            {
                lock (_lock)
                {
                    if(context == null)
                    {
                        context = new DatabaseContext();
                    }
                }
            }
        }
    }
}
