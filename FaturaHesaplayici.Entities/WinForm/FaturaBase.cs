﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.Entities.WinForm
{
    public abstract class FaturaBase
    {   
        public double SuankiAyinOrtakTuketimi { get; set; }
        public double SuankiAyinSayactakiTuketimi { get; set; }
        public int GecenAyinSecimDurumu { get; set; }
        public double GecenAyinSayactakiTuketimi { get; set; }
        public string GecenAyinSayactakiSecimiSelect { get; set; }
        public double SuankiAydaHarcananMiktar { get; set; }
        public double BirimFiyat { get; set; }
        public double KDV { get; set; }
        public double OdenecekTutar { get; set; }
    }
}
