﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.Entities.WinForm
{
    public class ElektrikFaturasi : FaturaBase
    {
        public string TekZamanBirimFiyatStatus { get; set; }
        public double TekZamanBirimFiyat { get; set; }
        public double DagitimBedeliBirimFiyat { get; set; }
        public string DagitimBedeliBirimFiyatStatus { get; set; }
        public double EnerjiFonuBedeli { get; set; }
        public double TrtPayiBedeli { get; set; }
        public double ElektveHvgTukVerBedeli { get; set; }
        public double ToplamOdenecekTutar { get; set; }
    }
}
