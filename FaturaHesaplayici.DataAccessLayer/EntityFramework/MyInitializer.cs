﻿using FaturaHesaplayici.Entities.DBSets;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.DataAccessLayer.EntityFramework
{
    public class MyInitializer :CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            ElektrikFaturasiDB e = new ElektrikFaturasiDB()
            {
                Ay = "Test",
                SayactakiOkunanMiktar = 266,
                DagitimBedeliBirimFiyat = 0.15408900,
                TekZamanBirimFiyat = 0.27909800
            };

            context.ElektrikFaturasi.Add(e);

            context.SaveChanges();
        }
    }
}
