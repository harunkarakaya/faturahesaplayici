﻿using FaturaHesaplayici.BusinessLayer.Abstract;
using FaturaHesaplayici.BusinessLayer.Result;
using FaturaHesaplayici.Entities.DBSets;
using FaturaHesaplayici.Entities.Messages;
using FaturaHesaplayici.Entities.WinForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.BusinessLayer
{
    public class SuManager : ManagerBase<SuFaturasiDB>
    {
        public new BusinessLayerResult<SuFaturasiDB> Insert(SuFaturasiDB suObj)
        {
            BusinessLayerResult<SuFaturasiDB> suResult = new BusinessLayerResult<SuFaturasiDB>();

            if(suObj != null)
            {
                if(suObj.Ay != "" && suObj.SayactakiOkunanMiktar != 0 && suObj.SuBirimFiyat != 0)
                {
                    SuFaturasiDB suDB = base.Find(s => s.Ay == suObj.Ay);

                    if(suDB != null)
                    {
                        suResult.MessageAdd("Kayıtlı olan fatura tekrardan kayıt edilemez. Kayıt başarısız!", MessageCode.FaturaBilgileriKayıtlıdır);

                        return suResult;
                    }
                    else
                    {
                        if(base.Insert(suObj) == 1)
                        {
                            suResult.Obj = suObj;
                            suResult.MessageAdd("Fatura bilgileri kayıt edildi.", MessageCode.FaturaBilgileriKayıtEdildi);

                            return suResult;
                        }

                    }
                }
                else
                {
                    suResult.Obj = suObj;
                    suResult.MessageAdd("Lütfen elektrik faturası bilgilerini eksiksiz doldurunuz.", MessageCode.FaturaBilgileriEksik);

                    return suResult;
                }
            }
            suResult.Obj = suObj;
            suResult.MessageAdd("Boş nesne!", MessageCode.BosNesne);

            return suResult;
        }

        public new BusinessLayerResult<SuFaturasiDB> List()
        {
            BusinessLayerResult<SuFaturasiDB> suResult = new BusinessLayerResult<SuFaturasiDB>();

            if(base.List().Count == 0)
            {
                suResult.MessageAdd("Kayıtlı fatura bilgisi bulunmuyor.", MessageCode.KayitliFaturaBilgisiBulunmuyor);
            }

            suResult.ListObj = base.List();

            return suResult;
        }

        public BusinessLayerResult<SuFaturasiDB> Update(double? yeniSuBirimFiyat)
        {
            BusinessLayerResult<SuFaturasiDB> suFaturasiDB = new BusinessLayerResult<SuFaturasiDB>();

            if(yeniSuBirimFiyat != 0 && yeniSuBirimFiyat != null)
            {
                List<SuFaturasiDB> guncellenecekSu = base.List();

                if (guncellenecekSu.Count > 0)
                {
                    SuFaturasiDB guncelSu = guncellenecekSu[guncellenecekSu.Count - 1];

                    guncelSu.SuBirimFiyat = yeniSuBirimFiyat.Value;

                    if (base.Update(guncelSu) == 1)
                    {
                        SuFaturasiDB guncelSuDB = Find(s => s.SuBirimFiyat == yeniSuBirimFiyat);

                        if (guncelSuDB != null)
                        {
                            suFaturasiDB.MessageAdd("Su birim fiyat güncellemesi başarılı", MessageCode.SuBirimFiyatGuncellendi);
                            suFaturasiDB.Obj = guncelSuDB;

                            return suFaturasiDB;
                        }
                        else
                        {
                            suFaturasiDB.MessageAdd("Güncelleme başarısız.", MessageCode.SuBirimFiyatGuncellemesiBasarisiz);

                            return suFaturasiDB;
                        }
                    }
                    else
                    {
                        suFaturasiDB.MessageAdd("Güncelleme başarısız.", MessageCode.SuBirimFiyatGuncellemesiBasarisiz);

                        return suFaturasiDB;
                    }
                }
                else
                {
                    suFaturasiDB.MessageAdd("Güncellenecek su birim fiyat bulunamadı.", MessageCode.GuncellenecekSuBirimFiyatBulunamadi);

                    return suFaturasiDB;
                }
            }
            else
            {
                suFaturasiDB.MessageAdd("Eski su birim fiyat ve güncellenecek birim fiyat boş",MessageCode.BosAlanlariDoldurunuz);

                return suFaturasiDB;
            }
        }

        public BusinessLayerResult<SuFaturasi> SuFaturasiHesapla(SuFaturasi su)
        {
            BusinessLayerResult<SuFaturasi> suFat = new BusinessLayerResult<SuFaturasi>();

            if(su != null)
            {
                if(su.BakimBedeli != 0 && su.BirimFiyat != 0 && su.CTV != 0 && su.KDV != 0 && su.SuankiAyinOrtakTuketimi != 0 && su.SuankiAyinSayactakiTuketimi != 0 && su.GecenAyinSecimDurumu != 0)
                {
                    if(su.GecenAyinSayactakiSecimiSelect != "" || su.GecenAyinSayactakiTuketimi != 0)
                    {
                        su.SuankiAydaHarcananMiktar = su.SuankiAyinSayactakiTuketimi - su.GecenAyinSayactakiTuketimi;
                 
                        su.OdenecekTutar += su.SuankiAydaHarcananMiktar * su.BirimFiyat;
                        su.OdenecekTutar += VergiveFonlarHesapla(su);

                        if(su.OdenecekTutar > 0)
                        {
                            suFat.Obj = su;
                            suFat.MessageAdd("Fatura başarıyla hesaplandı.", MessageCode.FaturaBasariylaHesaplandi);

                            return suFat;
                        }

                        suFat.MessageAdd("Lütfen girdiğiniz bilgileri kontrol ediniz.", MessageCode.GirilenRakamlariKontrolEdiniz);

                        return suFat;
                    }

                    suFat.MessageAdd("Lütfen gecen ayın tüketimini belirtiniz.", MessageCode.GecenAyinTuketimiBelirtilmedi);

                    return suFat;
                }

                suFat.MessageAdd("Lütfen boş alanları doldurunuz.", MessageCode.BosAlanlariDoldurunuz);

                return suFat;
            }

            suFat.MessageAdd("Lütfen boş alanları doldurunuz.", MessageCode.BosAlanlariDoldurunuz);

            return suFat;
        }

        public double VergiveFonlarHesapla(SuFaturasi su)
        {
            return BakimBedeliniHesapla(su) + KDVHesapla(su) + CTVHesapla(su);
        }

        public double BakimBedeliniHesapla(SuFaturasi su)
        {
            double BakimBedeli = 0, birmt3edusenTL = 0;

            birmt3edusenTL = su.BakimBedeli / su.SuankiAyinOrtakTuketimi;
            BakimBedeli = birmt3edusenTL * su.SuankiAydaHarcananMiktar;

            return BakimBedeli;
        }

        public double KDVHesapla(SuFaturasi su)
        {
            double KDV = 0, birmt3edusenTL = 0;

            birmt3edusenTL = su.KDV / su.SuankiAyinOrtakTuketimi;
            KDV = birmt3edusenTL * su.SuankiAydaHarcananMiktar;

            return KDV;
        }

        public double CTVHesapla(SuFaturasi su)
        {
            double CTV = 0, birmt3edusenTL = 0;

            birmt3edusenTL = su.CTV / su.SuankiAyinOrtakTuketimi;
            CTV = birmt3edusenTL * su.SuankiAydaHarcananMiktar;

            return CTV;
        }
    }
}
