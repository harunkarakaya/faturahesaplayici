﻿using FaturaHesaplayici.Entities.DBSets;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.DataAccessLayer.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseContext, FaturaHesaplayici.DataAccessLayer.Migrations.Configuration>());
        }

        public DbSet<SuFaturasiDB> SuFaturasi { get; set; }
        public DbSet<ElektrikFaturasiDB> ElektrikFaturasi { get; set; }
    }
}
