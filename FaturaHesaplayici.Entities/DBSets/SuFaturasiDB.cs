﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.Entities.DBSets
{
    [Table("SuFaturasi")]
    public class SuFaturasiDB : FaturaBaseDB
    {
        public SuFaturasiDB()
        {
        }

        public double SuBirimFiyat { get; set; }
    }
}
