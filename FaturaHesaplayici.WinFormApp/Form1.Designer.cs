﻿namespace FaturaHesaplayici.WinFormApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSuankiAyınOrtakTuketimKwh = new System.Windows.Forms.Label();
            this.txtSuankiAyınOrtakTuketimKwhı = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radBtnElektrikManuel = new System.Windows.Forms.RadioButton();
            this.radBtnElektrikListeden = new System.Windows.Forms.RadioButton();
            this.lblGecenAyinSayactakiKwhTuketimi = new System.Windows.Forms.Label();
            this.lblGecenAyinKwhSecimi = new System.Windows.Forms.Label();
            this.lblSuankiAyinSayactakiKwhTuketimi = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtGecenAyinSayactakiKwhTuketimi = new System.Windows.Forms.TextBox();
            this.txtSuankiAyinSayactakiKwhTuketimi = new System.Windows.Forms.TextBox();
            this.txtTekZamanBirimFiyati = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lstboxGecenAyinKwhSecimi = new System.Windows.Forms.ListBox();
            this.btnTekZamanBirimFiyatiGuncelle = new System.Windows.Forms.Button();
            this.lblSuankiAydaHarcadiginizKwhMiktari = new System.Windows.Forms.Label();
            this.btnElektrikFaturasiniHesapla = new System.Windows.Forms.Button();
            this.rdBtnAktifTekZamanBirimFiyati = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDagitimBedeliFiyati = new System.Windows.Forms.TextBox();
            this.btnDagitimBedeliFiyatiGuncelle = new System.Windows.Forms.Button();
            this.rdBtnDagitimBedeliFiyati = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblToplamOdenecekElektrikTutari = new System.Windows.Forms.Label();
            this.txtEnerjiFonuBedeli = new System.Windows.Forms.TextBox();
            this.txtTrtPayiBedeli = new System.Windows.Forms.TextBox();
            this.txtElektveHvgTukVerBedeli = new System.Windows.Forms.TextBox();
            this.txtElektrikKDV = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnElektrikFaturasiniKaydet = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblSuankiAydaHarcadiginiz = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSuFaturasiniKaydet = new System.Windows.Forms.Button();
            this.btnSuFaturasiniHesapla = new System.Windows.Forms.Button();
            this.lblToplamOdenecekSuTutari = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtCTVBedeli = new System.Windows.Forms.TextBox();
            this.txtKDVBedeli = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSuBakimBedeliFiyati = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.btnSuBirimFiyatiGuncelle = new System.Windows.Forms.Button();
            this.rdBtnAktifSuBirimFiyati = new System.Windows.Forms.RadioButton();
            this.txtSuBirimFiyati = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblSuankiAydaHarcadiginizMt3Miktari = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lstBoxGecenAyinMt3Secimi = new System.Windows.Forms.ListBox();
            this.txtGecenAyinSayactakiMt3Tuketimi = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.radBtnSuListeden = new System.Windows.Forms.RadioButton();
            this.radBtnSuManuel = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSuankiAyinSayactakiMt3Tuketimi = new System.Windows.Forms.TextBox();
            this.txtSuankiAyinOrtakTuketimMt3u = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSuankiAyınOrtakTuketimKwh
            // 
            this.lblSuankiAyınOrtakTuketimKwh.AutoSize = true;
            this.lblSuankiAyınOrtakTuketimKwh.Location = new System.Drawing.Point(6, 16);
            this.lblSuankiAyınOrtakTuketimKwh.Name = "lblSuankiAyınOrtakTuketimKwh";
            this.lblSuankiAyınOrtakTuketimKwh.Size = new System.Drawing.Size(167, 13);
            this.lblSuankiAyınOrtakTuketimKwh.TabIndex = 0;
            this.lblSuankiAyınOrtakTuketimKwh.Text = "Şuan ki Ayın Ortak Tüketim Kwh\'ı:";
            // 
            // txtSuankiAyınOrtakTuketimKwhı
            // 
            this.txtSuankiAyınOrtakTuketimKwhı.Location = new System.Drawing.Point(198, 13);
            this.txtSuankiAyınOrtakTuketimKwhı.Name = "txtSuankiAyınOrtakTuketimKwhı";
            this.txtSuankiAyınOrtakTuketimKwhı.Size = new System.Drawing.Size(100, 20);
            this.txtSuankiAyınOrtakTuketimKwhı.TabIndex = 1;
            this.txtSuankiAyınOrtakTuketimKwhı.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(-3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Geçen Ayın Kwh Seçimi";
            // 
            // radBtnElektrikManuel
            // 
            this.radBtnElektrikManuel.AutoSize = true;
            this.radBtnElektrikManuel.Location = new System.Drawing.Point(5, 21);
            this.radBtnElektrikManuel.Name = "radBtnElektrikManuel";
            this.radBtnElektrikManuel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radBtnElektrikManuel.Size = new System.Drawing.Size(60, 17);
            this.radBtnElektrikManuel.TabIndex = 4;
            this.radBtnElektrikManuel.TabStop = true;
            this.radBtnElektrikManuel.Text = "Manuel";
            this.radBtnElektrikManuel.UseVisualStyleBackColor = true;
            this.radBtnElektrikManuel.CheckedChanged += new System.EventHandler(this.radBtnElektrikManuel_CheckedChanged);
            // 
            // radBtnElektrikListeden
            // 
            this.radBtnElektrikListeden.AutoSize = true;
            this.radBtnElektrikListeden.Location = new System.Drawing.Point(95, 21);
            this.radBtnElektrikListeden.Name = "radBtnElektrikListeden";
            this.radBtnElektrikListeden.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radBtnElektrikListeden.Size = new System.Drawing.Size(65, 17);
            this.radBtnElektrikListeden.TabIndex = 5;
            this.radBtnElektrikListeden.TabStop = true;
            this.radBtnElektrikListeden.Text = "Listeden";
            this.radBtnElektrikListeden.UseVisualStyleBackColor = true;
            this.radBtnElektrikListeden.CheckedChanged += new System.EventHandler(this.radBtnElektrikListeden_CheckedChanged);
            // 
            // lblGecenAyinSayactakiKwhTuketimi
            // 
            this.lblGecenAyinSayactakiKwhTuketimi.AutoSize = true;
            this.lblGecenAyinSayactakiKwhTuketimi.Location = new System.Drawing.Point(6, 52);
            this.lblGecenAyinSayactakiKwhTuketimi.Name = "lblGecenAyinSayactakiKwhTuketimi";
            this.lblGecenAyinSayactakiKwhTuketimi.Size = new System.Drawing.Size(182, 13);
            this.lblGecenAyinSayactakiKwhTuketimi.TabIndex = 6;
            this.lblGecenAyinSayactakiKwhTuketimi.Text = "Geçen Ayın Sayaçtaki Kwh Tüketimi:";
            // 
            // lblGecenAyinKwhSecimi
            // 
            this.lblGecenAyinKwhSecimi.AutoSize = true;
            this.lblGecenAyinKwhSecimi.Location = new System.Drawing.Point(6, 91);
            this.lblGecenAyinKwhSecimi.Name = "lblGecenAyinKwhSecimi";
            this.lblGecenAyinKwhSecimi.Size = new System.Drawing.Size(123, 13);
            this.lblGecenAyinKwhSecimi.TabIndex = 7;
            this.lblGecenAyinKwhSecimi.Text = "Geçen Ayın Kwh Seçimi:";
            // 
            // lblSuankiAyinSayactakiKwhTuketimi
            // 
            this.lblSuankiAyinSayactakiKwhTuketimi.AutoSize = true;
            this.lblSuankiAyinSayactakiKwhTuketimi.Location = new System.Drawing.Point(6, 47);
            this.lblSuankiAyinSayactakiKwhTuketimi.Name = "lblSuankiAyinSayactakiKwhTuketimi";
            this.lblSuankiAyinSayactakiKwhTuketimi.Size = new System.Drawing.Size(186, 13);
            this.lblSuankiAyinSayactakiKwhTuketimi.TabIndex = 8;
            this.lblSuankiAyinSayactakiKwhTuketimi.Text = "Şuan ki Ayın Sayaçtaki Kwh Tüketimi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Tek zaman birim fiyatı:";
            // 
            // txtGecenAyinSayactakiKwhTuketimi
            // 
            this.txtGecenAyinSayactakiKwhTuketimi.Enabled = false;
            this.txtGecenAyinSayactakiKwhTuketimi.Location = new System.Drawing.Point(194, 49);
            this.txtGecenAyinSayactakiKwhTuketimi.Name = "txtGecenAyinSayactakiKwhTuketimi";
            this.txtGecenAyinSayactakiKwhTuketimi.Size = new System.Drawing.Size(100, 20);
            this.txtGecenAyinSayactakiKwhTuketimi.TabIndex = 10;
            this.txtGecenAyinSayactakiKwhTuketimi.Text = "0";
            this.txtGecenAyinSayactakiKwhTuketimi.TextChanged += new System.EventHandler(this.txtGecenAyinSayactakiKwhTuketimi_TextChanged);
            // 
            // txtSuankiAyinSayactakiKwhTuketimi
            // 
            this.txtSuankiAyinSayactakiKwhTuketimi.Location = new System.Drawing.Point(198, 44);
            this.txtSuankiAyinSayactakiKwhTuketimi.Name = "txtSuankiAyinSayactakiKwhTuketimi";
            this.txtSuankiAyinSayactakiKwhTuketimi.Size = new System.Drawing.Size(100, 20);
            this.txtSuankiAyinSayactakiKwhTuketimi.TabIndex = 12;
            this.txtSuankiAyinSayactakiKwhTuketimi.Text = "0";
            // 
            // txtTekZamanBirimFiyati
            // 
            this.txtTekZamanBirimFiyati.Enabled = false;
            this.txtTekZamanBirimFiyati.Location = new System.Drawing.Point(169, 30);
            this.txtTekZamanBirimFiyati.Name = "txtTekZamanBirimFiyati";
            this.txtTekZamanBirimFiyati.Size = new System.Drawing.Size(100, 20);
            this.txtTekZamanBirimFiyati.TabIndex = 13;
            this.txtTekZamanBirimFiyati.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(-3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 18);
            this.label7.TabIndex = 14;
            this.label7.Text = "ELEKTRİK FATURASI";
            // 
            // lstboxGecenAyinKwhSecimi
            // 
            this.lstboxGecenAyinKwhSecimi.Enabled = false;
            this.lstboxGecenAyinKwhSecimi.FormattingEnabled = true;
            this.lstboxGecenAyinKwhSecimi.Location = new System.Drawing.Point(134, 80);
            this.lstboxGecenAyinKwhSecimi.Name = "lstboxGecenAyinKwhSecimi";
            this.lstboxGecenAyinKwhSecimi.Size = new System.Drawing.Size(111, 56);
            this.lstboxGecenAyinKwhSecimi.TabIndex = 15;
            // 
            // btnTekZamanBirimFiyatiGuncelle
            // 
            this.btnTekZamanBirimFiyatiGuncelle.Location = new System.Drawing.Point(327, 26);
            this.btnTekZamanBirimFiyatiGuncelle.Name = "btnTekZamanBirimFiyatiGuncelle";
            this.btnTekZamanBirimFiyatiGuncelle.Size = new System.Drawing.Size(183, 27);
            this.btnTekZamanBirimFiyatiGuncelle.TabIndex = 16;
            this.btnTekZamanBirimFiyatiGuncelle.Text = "Tek zaman birim fiyatını güncelle";
            this.btnTekZamanBirimFiyatiGuncelle.UseVisualStyleBackColor = true;
            this.btnTekZamanBirimFiyatiGuncelle.Click += new System.EventHandler(this.btnTekZamanBirimFiyatiGuncelle_Click);
            // 
            // lblSuankiAydaHarcadiginizKwhMiktari
            // 
            this.lblSuankiAydaHarcadiginizKwhMiktari.AutoSize = true;
            this.lblSuankiAydaHarcadiginizKwhMiktari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSuankiAydaHarcadiginizKwhMiktari.Location = new System.Drawing.Point(287, 247);
            this.lblSuankiAydaHarcadiginizKwhMiktari.Name = "lblSuankiAydaHarcadiginizKwhMiktari";
            this.lblSuankiAydaHarcadiginizKwhMiktari.Size = new System.Drawing.Size(34, 16);
            this.lblSuankiAydaHarcadiginizKwhMiktari.TabIndex = 18;
            this.lblSuankiAydaHarcadiginizKwhMiktari.Text = "kwh";
            // 
            // btnElektrikFaturasiniHesapla
            // 
            this.btnElektrikFaturasiniHesapla.Location = new System.Drawing.Point(40, 587);
            this.btnElektrikFaturasiniHesapla.Name = "btnElektrikFaturasiniHesapla";
            this.btnElektrikFaturasiniHesapla.Size = new System.Drawing.Size(154, 58);
            this.btnElektrikFaturasiniHesapla.TabIndex = 19;
            this.btnElektrikFaturasiniHesapla.Text = "Elektrik Faturasını Hesapla";
            this.btnElektrikFaturasiniHesapla.UseVisualStyleBackColor = true;
            this.btnElektrikFaturasiniHesapla.Click += new System.EventHandler(this.btnElektrikFaturasiniHesapla_Click);
            // 
            // rdBtnAktifTekZamanBirimFiyati
            // 
            this.rdBtnAktifTekZamanBirimFiyati.AutoSize = true;
            this.rdBtnAktifTekZamanBirimFiyati.Location = new System.Drawing.Point(275, 31);
            this.rdBtnAktifTekZamanBirimFiyati.Name = "rdBtnAktifTekZamanBirimFiyati";
            this.rdBtnAktifTekZamanBirimFiyati.Size = new System.Drawing.Size(46, 17);
            this.rdBtnAktifTekZamanBirimFiyati.TabIndex = 20;
            this.rdBtnAktifTekZamanBirimFiyati.Text = "Aktif";
            this.rdBtnAktifTekZamanBirimFiyati.UseVisualStyleBackColor = true;
            this.rdBtnAktifTekZamanBirimFiyati.CheckedChanged += new System.EventHandler(this.rdBtnAktifTekZamanBirimFiyati_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Dağıtım bedeli birim fiyatı:";
            // 
            // txtDagitimBedeliFiyati
            // 
            this.txtDagitimBedeliFiyati.Enabled = false;
            this.txtDagitimBedeliFiyati.Location = new System.Drawing.Point(169, 66);
            this.txtDagitimBedeliFiyati.Name = "txtDagitimBedeliFiyati";
            this.txtDagitimBedeliFiyati.Size = new System.Drawing.Size(100, 20);
            this.txtDagitimBedeliFiyati.TabIndex = 13;
            this.txtDagitimBedeliFiyati.Text = "0";
            // 
            // btnDagitimBedeliFiyatiGuncelle
            // 
            this.btnDagitimBedeliFiyatiGuncelle.Location = new System.Drawing.Point(327, 59);
            this.btnDagitimBedeliFiyatiGuncelle.Name = "btnDagitimBedeliFiyatiGuncelle";
            this.btnDagitimBedeliFiyatiGuncelle.Size = new System.Drawing.Size(183, 27);
            this.btnDagitimBedeliFiyatiGuncelle.TabIndex = 16;
            this.btnDagitimBedeliFiyatiGuncelle.Text = "Dağıtım bedeli birim fiyatını güncelle";
            this.btnDagitimBedeliFiyatiGuncelle.UseVisualStyleBackColor = true;
            this.btnDagitimBedeliFiyatiGuncelle.Click += new System.EventHandler(this.btnDagitimBedeliFiyatiGuncelle_Click);
            // 
            // rdBtnDagitimBedeliFiyati
            // 
            this.rdBtnDagitimBedeliFiyati.AutoSize = true;
            this.rdBtnDagitimBedeliFiyati.Location = new System.Drawing.Point(275, 67);
            this.rdBtnDagitimBedeliFiyati.Name = "rdBtnDagitimBedeliFiyati";
            this.rdBtnDagitimBedeliFiyati.Size = new System.Drawing.Size(46, 17);
            this.rdBtnDagitimBedeliFiyati.TabIndex = 20;
            this.rdBtnDagitimBedeliFiyati.TabStop = true;
            this.rdBtnDagitimBedeliFiyati.Text = "Aktif";
            this.rdBtnDagitimBedeliFiyati.UseVisualStyleBackColor = true;
            this.rdBtnDagitimBedeliFiyati.CheckedChanged += new System.EventHandler(this.rdBtnDagitimBedeliFiyati_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Enerji Fonu Bedeli:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "TRT Payı Bedeli:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Elekt. ve Hvg. Tük. Ver. Bedeli:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "KDV %18:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(8, 549);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(243, 16);
            this.label9.TabIndex = 25;
            this.label9.Text = "Ödenecek Elektrik Faturası Tutarı:";
            // 
            // lblToplamOdenecekElektrikTutari
            // 
            this.lblToplamOdenecekElektrikTutari.AutoSize = true;
            this.lblToplamOdenecekElektrikTutari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblToplamOdenecekElektrikTutari.Location = new System.Drawing.Point(257, 549);
            this.lblToplamOdenecekElektrikTutari.Name = "lblToplamOdenecekElektrikTutari";
            this.lblToplamOdenecekElektrikTutari.Size = new System.Drawing.Size(236, 16);
            this.lblToplamOdenecekElektrikTutari.TabIndex = 26;
            this.lblToplamOdenecekElektrikTutari.Text = "Toplam Ödenecek Elektrik Tutarı";
            // 
            // txtEnerjiFonuBedeli
            // 
            this.txtEnerjiFonuBedeli.Location = new System.Drawing.Point(169, 104);
            this.txtEnerjiFonuBedeli.Name = "txtEnerjiFonuBedeli";
            this.txtEnerjiFonuBedeli.Size = new System.Drawing.Size(100, 20);
            this.txtEnerjiFonuBedeli.TabIndex = 27;
            this.txtEnerjiFonuBedeli.Text = "0";
            // 
            // txtTrtPayiBedeli
            // 
            this.txtTrtPayiBedeli.Location = new System.Drawing.Point(169, 140);
            this.txtTrtPayiBedeli.Name = "txtTrtPayiBedeli";
            this.txtTrtPayiBedeli.Size = new System.Drawing.Size(100, 20);
            this.txtTrtPayiBedeli.TabIndex = 28;
            this.txtTrtPayiBedeli.Text = "0";
            // 
            // txtElektveHvgTukVerBedeli
            // 
            this.txtElektveHvgTukVerBedeli.Location = new System.Drawing.Point(169, 177);
            this.txtElektveHvgTukVerBedeli.Name = "txtElektveHvgTukVerBedeli";
            this.txtElektveHvgTukVerBedeli.Size = new System.Drawing.Size(100, 20);
            this.txtElektveHvgTukVerBedeli.TabIndex = 29;
            this.txtElektveHvgTukVerBedeli.Text = "0";
            // 
            // txtElektrikKDV
            // 
            this.txtElektrikKDV.Location = new System.Drawing.Point(169, 213);
            this.txtElektrikKDV.Name = "txtElektrikKDV";
            this.txtElektrikKDV.Size = new System.Drawing.Size(100, 20);
            this.txtElektrikKDV.TabIndex = 30;
            this.txtElektrikKDV.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(-4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 20);
            this.label11.TabIndex = 31;
            this.label11.Text = "Fatura";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblSuankiAyınOrtakTuketimKwh);
            this.groupBox1.Controls.Add(this.txtSuankiAyınOrtakTuketimKwhı);
            this.groupBox1.Controls.Add(this.lblSuankiAyinSayactakiKwhTuketimi);
            this.groupBox1.Controls.Add(this.txtSuankiAyinSayactakiKwhTuketimi);
            this.groupBox1.Location = new System.Drawing.Point(6, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(312, 73);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.lblGecenAyinSayactakiKwhTuketimi);
            this.groupBox2.Controls.Add(this.radBtnElektrikManuel);
            this.groupBox2.Controls.Add(this.radBtnElektrikListeden);
            this.groupBox2.Controls.Add(this.lblGecenAyinKwhSecimi);
            this.groupBox2.Controls.Add(this.txtGecenAyinSayactakiKwhTuketimi);
            this.groupBox2.Controls.Add(this.lstboxGecenAyinKwhSecimi);
            this.groupBox2.Location = new System.Drawing.Point(6, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(312, 144);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.btnTekZamanBirimFiyatiGuncelle);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtElektrikKDV);
            this.groupBox3.Controls.Add(this.txtTekZamanBirimFiyati);
            this.groupBox3.Controls.Add(this.txtElektveHvgTukVerBedeli);
            this.groupBox3.Controls.Add(this.rdBtnAktifTekZamanBirimFiyati);
            this.groupBox3.Controls.Add(this.txtTrtPayiBedeli);
            this.groupBox3.Controls.Add(this.btnDagitimBedeliFiyatiGuncelle);
            this.groupBox3.Controls.Add(this.txtEnerjiFonuBedeli);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtDagitimBedeliFiyati);
            this.groupBox3.Controls.Add(this.rdBtnDagitimBedeliFiyati);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(6, 270);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(518, 256);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            // 
            // btnElektrikFaturasiniKaydet
            // 
            this.btnElektrikFaturasiniKaydet.Location = new System.Drawing.Point(260, 587);
            this.btnElektrikFaturasiniKaydet.Name = "btnElektrikFaturasiniKaydet";
            this.btnElektrikFaturasiniKaydet.Size = new System.Drawing.Size(158, 58);
            this.btnElektrikFaturasiniKaydet.TabIndex = 35;
            this.btnElektrikFaturasiniKaydet.Text = "Elektrik Faturasını Kaydet";
            this.btnElektrikFaturasiniKaydet.UseVisualStyleBackColor = true;
            this.btnElektrikFaturasiniKaydet.Click += new System.EventHandler(this.btnElektrikFaturasiniKaydet_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblSuankiAydaHarcadiginiz);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.btnElektrikFaturasiniKaydet);
            this.groupBox4.Controls.Add(this.groupBox1);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.lblSuankiAydaHarcadiginizKwhMiktari);
            this.groupBox4.Controls.Add(this.btnElektrikFaturasiniHesapla);
            this.groupBox4.Controls.Add(this.lblToplamOdenecekElektrikTutari);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(537, 688);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            // 
            // lblSuankiAydaHarcadiginiz
            // 
            this.lblSuankiAydaHarcadiginiz.AutoSize = true;
            this.lblSuankiAydaHarcadiginiz.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSuankiAydaHarcadiginiz.Location = new System.Drawing.Point(6, 247);
            this.lblSuankiAydaHarcadiginiz.Name = "lblSuankiAydaHarcadiginiz";
            this.lblSuankiAydaHarcadiginiz.Size = new System.Drawing.Size(275, 16);
            this.lblSuankiAydaHarcadiginiz.TabIndex = 36;
            this.lblSuankiAydaHarcadiginiz.Text = "Şuan ki Ayda Harcadığınız Khw Miktarı:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnSuFaturasiniKaydet);
            this.groupBox5.Controls.Add(this.btnSuFaturasiniHesapla);
            this.groupBox5.Controls.Add(this.lblToplamOdenecekSuTutari);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.lblSuankiAydaHarcadiginizMt3Miktari);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Location = new System.Drawing.Point(564, 15);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(446, 685);
            this.groupBox5.TabIndex = 37;
            this.groupBox5.TabStop = false;
            // 
            // btnSuFaturasiniKaydet
            // 
            this.btnSuFaturasiniKaydet.Location = new System.Drawing.Point(260, 584);
            this.btnSuFaturasiniKaydet.Name = "btnSuFaturasiniKaydet";
            this.btnSuFaturasiniKaydet.Size = new System.Drawing.Size(119, 56);
            this.btnSuFaturasiniKaydet.TabIndex = 13;
            this.btnSuFaturasiniKaydet.Text = "Su Faturasını Kaydet";
            this.btnSuFaturasiniKaydet.UseVisualStyleBackColor = true;
            this.btnSuFaturasiniKaydet.Click += new System.EventHandler(this.btnSuFaturasiniKaydet_Click);
            // 
            // btnSuFaturasiniHesapla
            // 
            this.btnSuFaturasiniHesapla.Location = new System.Drawing.Point(54, 584);
            this.btnSuFaturasiniHesapla.Name = "btnSuFaturasiniHesapla";
            this.btnSuFaturasiniHesapla.Size = new System.Drawing.Size(123, 56);
            this.btnSuFaturasiniHesapla.TabIndex = 12;
            this.btnSuFaturasiniHesapla.Text = "Su Faturasını Hesapla";
            this.btnSuFaturasiniHesapla.UseVisualStyleBackColor = true;
            this.btnSuFaturasiniHesapla.Click += new System.EventHandler(this.btnSuFaturasiniHesapla_Click);
            // 
            // lblToplamOdenecekSuTutari
            // 
            this.lblToplamOdenecekSuTutari.AutoSize = true;
            this.lblToplamOdenecekSuTutari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblToplamOdenecekSuTutari.Location = new System.Drawing.Point(230, 548);
            this.lblToplamOdenecekSuTutari.Name = "lblToplamOdenecekSuTutari";
            this.lblToplamOdenecekSuTutari.Size = new System.Drawing.Size(202, 16);
            this.lblToplamOdenecekSuTutari.TabIndex = 11;
            this.lblToplamOdenecekSuTutari.Text = "Toplam Ödenecek Su Tutarı";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label25.Location = new System.Drawing.Point(9, 546);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(209, 16);
            this.label25.TabIndex = 10;
            this.label25.Text = "Ödenecek Su Faturası Tutarı:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtCTVBedeli);
            this.groupBox8.Controls.Add(this.txtKDVBedeli);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.txtSuBakimBedeliFiyati);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.btnSuBirimFiyatiGuncelle);
            this.groupBox8.Controls.Add(this.rdBtnAktifSuBirimFiyati);
            this.groupBox8.Controls.Add(this.txtSuBirimFiyati);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Location = new System.Drawing.Point(6, 267);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(430, 256);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Enter += new System.EventHandler(this.groupBox8_Enter);
            // 
            // txtCTVBedeli
            // 
            this.txtCTVBedeli.Location = new System.Drawing.Point(121, 140);
            this.txtCTVBedeli.Name = "txtCTVBedeli";
            this.txtCTVBedeli.Size = new System.Drawing.Size(100, 20);
            this.txtCTVBedeli.TabIndex = 16;
            this.txtCTVBedeli.Text = "0";
            // 
            // txtKDVBedeli
            // 
            this.txtKDVBedeli.Location = new System.Drawing.Point(121, 100);
            this.txtKDVBedeli.Name = "txtKDVBedeli";
            this.txtKDVBedeli.Size = new System.Drawing.Size(100, 20);
            this.txtKDVBedeli.TabIndex = 15;
            this.txtKDVBedeli.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 143);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(63, 13);
            this.label24.TabIndex = 14;
            this.label24.Text = "ÇTV Bedeli:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 107);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(64, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "KDV Bedeli:";
            // 
            // txtSuBakimBedeliFiyati
            // 
            this.txtSuBakimBedeliFiyati.Location = new System.Drawing.Point(121, 66);
            this.txtSuBakimBedeliFiyati.Name = "txtSuBakimBedeliFiyati";
            this.txtSuBakimBedeliFiyati.Size = new System.Drawing.Size(100, 20);
            this.txtSuBakimBedeliFiyati.TabIndex = 12;
            this.txtSuBakimBedeliFiyati.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 69);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(109, 13);
            this.label22.TabIndex = 11;
            this.label22.Text = "Su bakım bedeli fiyatı:";
            // 
            // btnSuBirimFiyatiGuncelle
            // 
            this.btnSuBirimFiyatiGuncelle.Location = new System.Drawing.Point(279, 26);
            this.btnSuBirimFiyatiGuncelle.Name = "btnSuBirimFiyatiGuncelle";
            this.btnSuBirimFiyatiGuncelle.Size = new System.Drawing.Size(138, 27);
            this.btnSuBirimFiyatiGuncelle.TabIndex = 10;
            this.btnSuBirimFiyatiGuncelle.Text = "Su birim fiyatını güncelle";
            this.btnSuBirimFiyatiGuncelle.UseVisualStyleBackColor = true;
            this.btnSuBirimFiyatiGuncelle.Click += new System.EventHandler(this.btnSuBirimFiyatiGuncelle_Click);
            // 
            // rdBtnAktifSuBirimFiyati
            // 
            this.rdBtnAktifSuBirimFiyati.AutoSize = true;
            this.rdBtnAktifSuBirimFiyati.Location = new System.Drawing.Point(227, 31);
            this.rdBtnAktifSuBirimFiyati.Name = "rdBtnAktifSuBirimFiyati";
            this.rdBtnAktifSuBirimFiyati.Size = new System.Drawing.Size(46, 17);
            this.rdBtnAktifSuBirimFiyati.TabIndex = 3;
            this.rdBtnAktifSuBirimFiyati.TabStop = true;
            this.rdBtnAktifSuBirimFiyati.Text = "Aktif";
            this.rdBtnAktifSuBirimFiyati.UseVisualStyleBackColor = true;
            this.rdBtnAktifSuBirimFiyati.CheckedChanged += new System.EventHandler(this.rdBtnAktifSuBirimFiyati_CheckedChanged);
            // 
            // txtSuBirimFiyati
            // 
            this.txtSuBirimFiyati.Enabled = false;
            this.txtSuBirimFiyati.Location = new System.Drawing.Point(121, 30);
            this.txtSuBirimFiyati.Name = "txtSuBirimFiyati";
            this.txtSuBirimFiyati.Size = new System.Drawing.Size(100, 20);
            this.txtSuBirimFiyati.TabIndex = 2;
            this.txtSuBirimFiyati.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Su birim fiyatı:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label20.Location = new System.Drawing.Point(-4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 20);
            this.label20.TabIndex = 0;
            this.label20.Text = "Fatura";
            // 
            // lblSuankiAydaHarcadiginizMt3Miktari
            // 
            this.lblSuankiAydaHarcadiginizMt3Miktari.AutoSize = true;
            this.lblSuankiAydaHarcadiginizMt3Miktari.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSuankiAydaHarcadiginizMt3Miktari.Location = new System.Drawing.Point(299, 249);
            this.lblSuankiAydaHarcadiginizMt3Miktari.Name = "lblSuankiAydaHarcadiginizMt3Miktari";
            this.lblSuankiAydaHarcadiginizMt3Miktari.Size = new System.Drawing.Size(40, 16);
            this.lblSuankiAydaHarcadiginizMt3Miktari.TabIndex = 8;
            this.lblSuankiAydaHarcadiginizMt3Miktari.Text = "mt^3";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label18.Location = new System.Drawing.Point(15, 247);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(280, 16);
            this.label18.TabIndex = 7;
            this.label18.Text = "Şuan ki Ayda Harcadığınız Mt^3 Miktarı:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lstBoxGecenAyinMt3Secimi);
            this.groupBox7.Controls.Add(this.txtGecenAyinSayactakiMt3Tuketimi);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.radBtnSuListeden);
            this.groupBox7.Controls.Add(this.radBtnSuManuel);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Location = new System.Drawing.Point(6, 97);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(316, 144);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            // 
            // lstBoxGecenAyinMt3Secimi
            // 
            this.lstBoxGecenAyinMt3Secimi.Enabled = false;
            this.lstBoxGecenAyinMt3Secimi.FormattingEnabled = true;
            this.lstBoxGecenAyinMt3Secimi.Location = new System.Drawing.Point(138, 80);
            this.lstBoxGecenAyinMt3Secimi.Name = "lstBoxGecenAyinMt3Secimi";
            this.lstBoxGecenAyinMt3Secimi.Size = new System.Drawing.Size(107, 56);
            this.lstBoxGecenAyinMt3Secimi.TabIndex = 13;
            // 
            // txtGecenAyinSayactakiMt3Tuketimi
            // 
            this.txtGecenAyinSayactakiMt3Tuketimi.Enabled = false;
            this.txtGecenAyinSayactakiMt3Tuketimi.Location = new System.Drawing.Point(197, 49);
            this.txtGecenAyinSayactakiMt3Tuketimi.Name = "txtGecenAyinSayactakiMt3Tuketimi";
            this.txtGecenAyinSayactakiMt3Tuketimi.Size = new System.Drawing.Size(100, 20);
            this.txtGecenAyinSayactakiMt3Tuketimi.TabIndex = 12;
            this.txtGecenAyinSayactakiMt3Tuketimi.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(126, 13);
            this.label17.TabIndex = 11;
            this.label17.Text = "Geçen Ayın Mt^3 Seçimi:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 52);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(185, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Geçen Ayın Sayaçtaki Mt^3 Tüketimi:";
            // 
            // radBtnSuListeden
            // 
            this.radBtnSuListeden.AutoSize = true;
            this.radBtnSuListeden.Location = new System.Drawing.Point(99, 21);
            this.radBtnSuListeden.Name = "radBtnSuListeden";
            this.radBtnSuListeden.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radBtnSuListeden.Size = new System.Drawing.Size(65, 17);
            this.radBtnSuListeden.TabIndex = 9;
            this.radBtnSuListeden.TabStop = true;
            this.radBtnSuListeden.Text = "Listeden";
            this.radBtnSuListeden.UseVisualStyleBackColor = true;
            this.radBtnSuListeden.CheckedChanged += new System.EventHandler(this.radBtnSuListeden_CheckedChanged);
            // 
            // radBtnSuManuel
            // 
            this.radBtnSuManuel.AutoSize = true;
            this.radBtnSuManuel.Location = new System.Drawing.Point(6, 21);
            this.radBtnSuManuel.Name = "radBtnSuManuel";
            this.radBtnSuManuel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radBtnSuManuel.Size = new System.Drawing.Size(60, 17);
            this.radBtnSuManuel.TabIndex = 8;
            this.radBtnSuManuel.TabStop = true;
            this.radBtnSuManuel.Text = "Manuel";
            this.radBtnSuManuel.UseVisualStyleBackColor = true;
            this.radBtnSuManuel.CheckedChanged += new System.EventHandler(this.radBtnSuManuel_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.Location = new System.Drawing.Point(-3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(174, 16);
            this.label15.TabIndex = 7;
            this.label15.Text = "Geçen Ayın Mt^3 Seçimi";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.txtSuankiAyinSayactakiMt3Tuketimi);
            this.groupBox6.Controls.Add(this.txtSuankiAyinOrtakTuketimMt3u);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Location = new System.Drawing.Point(6, 17);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(316, 77);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(174, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Şuan ki Ayın Ortak Tüketim Mt^3\'ü:";
            // 
            // txtSuankiAyinSayactakiMt3Tuketimi
            // 
            this.txtSuankiAyinSayactakiMt3Tuketimi.Location = new System.Drawing.Point(201, 45);
            this.txtSuankiAyinSayactakiMt3Tuketimi.Name = "txtSuankiAyinSayactakiMt3Tuketimi";
            this.txtSuankiAyinSayactakiMt3Tuketimi.Size = new System.Drawing.Size(100, 20);
            this.txtSuankiAyinSayactakiMt3Tuketimi.TabIndex = 4;
            this.txtSuankiAyinSayactakiMt3Tuketimi.Text = "0";
            // 
            // txtSuankiAyinOrtakTuketimMt3u
            // 
            this.txtSuankiAyinOrtakTuketimMt3u.Location = new System.Drawing.Point(201, 13);
            this.txtSuankiAyinOrtakTuketimMt3u.Name = "txtSuankiAyinOrtakTuketimMt3u";
            this.txtSuankiAyinOrtakTuketimMt3u.Size = new System.Drawing.Size(100, 20);
            this.txtSuankiAyinOrtakTuketimMt3u.TabIndex = 3;
            this.txtSuankiAyinOrtakTuketimMt3u.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(189, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Şuan ki Ayın Sayaçtaki Mt^3 Tüketimi:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.Location = new System.Drawing.Point(-3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 18);
            this.label12.TabIndex = 0;
            this.label12.Text = "SU FATURASI";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1024, 712);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Name = "Form1";
            this.Text = "FATURA HESAPLAYICI";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblSuankiAyınOrtakTuketimKwh;
        private System.Windows.Forms.TextBox txtSuankiAyınOrtakTuketimKwhı;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radBtnElektrikManuel;
        private System.Windows.Forms.RadioButton radBtnElektrikListeden;
        private System.Windows.Forms.Label lblGecenAyinSayactakiKwhTuketimi;
        private System.Windows.Forms.Label lblGecenAyinKwhSecimi;
        private System.Windows.Forms.Label lblSuankiAyinSayactakiKwhTuketimi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtGecenAyinSayactakiKwhTuketimi;
        private System.Windows.Forms.TextBox txtSuankiAyinSayactakiKwhTuketimi;
        private System.Windows.Forms.TextBox txtTekZamanBirimFiyati;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lstboxGecenAyinKwhSecimi;
        private System.Windows.Forms.Button btnTekZamanBirimFiyatiGuncelle;
        private System.Windows.Forms.Label lblSuankiAydaHarcadiginizKwhMiktari;
        private System.Windows.Forms.Button btnElektrikFaturasiniHesapla;
        private System.Windows.Forms.RadioButton rdBtnAktifTekZamanBirimFiyati;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDagitimBedeliFiyati;
        private System.Windows.Forms.Button btnDagitimBedeliFiyatiGuncelle;
        private System.Windows.Forms.RadioButton rdBtnDagitimBedeliFiyati;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblToplamOdenecekElektrikTutari;
        private System.Windows.Forms.TextBox txtEnerjiFonuBedeli;
        private System.Windows.Forms.TextBox txtTrtPayiBedeli;
        private System.Windows.Forms.TextBox txtElektveHvgTukVerBedeli;
        private System.Windows.Forms.TextBox txtElektrikKDV;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnElektrikFaturasiniKaydet;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSuankiAyinSayactakiMt3Tuketimi;
        private System.Windows.Forms.TextBox txtSuankiAyinOrtakTuketimMt3u;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtGecenAyinSayactakiMt3Tuketimi;
        private System.Windows.Forms.ListBox lstBoxGecenAyinMt3Secimi;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton radBtnSuListeden;
        private System.Windows.Forms.RadioButton radBtnSuManuel;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnSuBirimFiyatiGuncelle;
        private System.Windows.Forms.RadioButton rdBtnAktifSuBirimFiyati;
        private System.Windows.Forms.TextBox txtSuBirimFiyati;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblSuankiAydaHarcadiginizMt3Miktari;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnSuFaturasiniKaydet;
        private System.Windows.Forms.Button btnSuFaturasiniHesapla;
        private System.Windows.Forms.Label lblToplamOdenecekSuTutari;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCTVBedeli;
        private System.Windows.Forms.TextBox txtKDVBedeli;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSuBakimBedeliFiyati;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblSuankiAydaHarcadiginiz;
    }
}

