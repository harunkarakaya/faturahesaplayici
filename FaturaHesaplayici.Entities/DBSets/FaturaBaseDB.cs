﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.Entities.DBSets
{
    public abstract class FaturaBaseDB
    {
        public FaturaBaseDB()
        {

        }

        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Ay { get; set; }
        public double SayactakiOkunanMiktar { get; set; }

    }
}
