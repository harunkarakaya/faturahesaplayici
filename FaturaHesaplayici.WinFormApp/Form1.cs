﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FaturaHesaplayici.BusinessLayer;
using FaturaHesaplayici.Entities.DBSets;
using FaturaHesaplayici.Entities.WinForm;
using FaturaHesaplayici.BusinessLayer.Result;

namespace FaturaHesaplayici.WinFormApp
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GecenAyinBirimveDagitimBilgileriniGetir(false);
            GecmisAylarinElektrikFaturasiBilgileriniGetir();

            GecenAyinSuBirimFiyatiniGetir(false);
            GecmisAylarinSuFaturasiBilgileriniGetir();
        }

        private void btnElektrikFaturasiniHesapla_Click(object sender, EventArgs e)
        {
            ElektrikFaturasi elektrik = new ElektrikFaturasi();
            ElektrikManager elektrikManager = new ElektrikManager();

            elektrik.SuankiAyinOrtakTuketimi = (txtSuankiAyınOrtakTuketimKwhı.Text != "") ? Convert.ToDouble(txtSuankiAyınOrtakTuketimKwhı.Text) : 0;
            elektrik.SuankiAyinSayactakiTuketimi = (txtSuankiAyinSayactakiKwhTuketimi.Text != "") ? Convert.ToDouble(txtSuankiAyinSayactakiKwhTuketimi.Text) : 0;

            //Edit will done
            //--------------------------------------------------------------------------------------------------------------------------------------------------------
            if (radBtnElektrikManuel.Checked == true)
            {
                elektrik.GecenAyinSecimDurumu = (int)ManuelListedenSecim.Manuel;
                elektrik.GecenAyinSayactakiTuketimi = (txtGecenAyinSayactakiKwhTuketimi.Text != "") ? Convert.ToDouble(txtGecenAyinSayactakiKwhTuketimi.Text) : 0;
            }
            else if(radBtnElektrikListeden.Checked == true)  
            {
                elektrik.GecenAyinSecimDurumu = (int)ManuelListedenSecim.Listeden;

                if (lstboxGecenAyinKwhSecimi.SelectedItem != null)
                {
                    elektrik.GecenAyinSayactakiSecimiSelect = lstboxGecenAyinKwhSecimi.SelectedItem.ToString();
                    ElektrikFaturasiDB AyKwh = elektrikManager.Find(f => f.Ay == elektrik.GecenAyinSayactakiSecimiSelect);

                    elektrik.GecenAyinSayactakiTuketimi = Convert.ToDouble(AyKwh.SayactakiOkunanMiktar);
                }
                else
                {
                    elektrik.GecenAyinSayactakiSecimiSelect = "secimyok";
                }
            }
            else
            {
                elektrik.GecenAyinSecimDurumu = (int)ManuelListedenSecim.SecimYok;
            }
            //--------------------------------------------------------------------------------------------------------------------------------------------------------

            elektrik.TekZamanBirimFiyat = (txtTekZamanBirimFiyati.Text != "") ? Convert.ToDouble(txtTekZamanBirimFiyati.Text) : 0;
            elektrik.DagitimBedeliBirimFiyat = (txtDagitimBedeliFiyati.Text != "") ? Convert.ToDouble(txtDagitimBedeliFiyati.Text) : 0;
            elektrik.EnerjiFonuBedeli = (txtEnerjiFonuBedeli.Text != "") ? Convert.ToDouble(txtEnerjiFonuBedeli.Text) : 0;
            elektrik.TrtPayiBedeli = (txtTrtPayiBedeli.Text != "") ? Convert.ToDouble(txtTrtPayiBedeli.Text) : 0;
            elektrik.ElektveHvgTukVerBedeli = (txtElektveHvgTukVerBedeli.Text != "") ? Convert.ToDouble(txtElektveHvgTukVerBedeli.Text) : 0;
            elektrik.KDV = (txtElektrikKDV.Text != "") ? Convert.ToDouble(txtElektrikKDV.Text) : 0;

            //Hesaplanan fatura bilgisi geliyor.
            BusinessLayerResult<ElektrikFaturasi> eFatResult = elektrikManager.FaturaTutariniHesapla(elektrik);
            if(eFatResult.Obj != null)
            {
                ElektrikFaturasi eResult = eFatResult.Obj;

                lblSuankiAydaHarcadiginizKwhMiktari.Text = Math.Round(eResult.SuankiAydaHarcananMiktar, 3).ToString() + " KHW";
                lblToplamOdenecekElektrikTutari.Text = Math.Round(eResult.ToplamOdenecekTutar, 2).ToString() + " TL";

                MessageBox.Show(eFatResult.messagesObj[0].Message.ToString());
            }
            else
            {
                MessageBox.Show(eFatResult.messagesObj[0].Message.ToString());
            }
        }

        private void rdBtnAktifTekZamanBirimFiyati_CheckedChanged(object sender, EventArgs e)
        {
            if(rdBtnAktifTekZamanBirimFiyati.Checked == true)
            {
                txtTekZamanBirimFiyati.Enabled = true;
            }
            else
            {
                txtTekZamanBirimFiyati.Enabled = false;
            }
        }

        private void radBtnElektrikManuel_CheckedChanged(object sender, EventArgs e)
        {
            if (radBtnElektrikManuel.Checked == true)
            {
                txtGecenAyinSayactakiKwhTuketimi.Enabled = true;
                lstboxGecenAyinKwhSecimi.SelectedItems.Clear();
            }
            else
            {
                txtGecenAyinSayactakiKwhTuketimi.Enabled = false;
            }
        }

        private void radBtnElektrikListeden_CheckedChanged(object sender, EventArgs e)
        {
            if(radBtnElektrikListeden.Checked == true)
            {
                lstboxGecenAyinKwhSecimi.Enabled = true;
            }
            else
            {
                lstboxGecenAyinKwhSecimi.Enabled = false;
            }
        }

        private void txtGecenAyinSayactakiKwhTuketimi_TextChanged(object sender, EventArgs e)
        {

        }

        private void rdBtnDagitimBedeliFiyati_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnDagitimBedeliFiyati.Checked == true)
            {
                txtDagitimBedeliFiyati.Enabled = true;
            }
            else
            {
                txtDagitimBedeliFiyati.Enabled = false;
            }
        }

        private void btnElektrikFaturasiniKaydet_Click(object sender, EventArgs e)
        {
            ElektrikManager ElektMngr = new ElektrikManager();

            ElektrikFaturasiDB elektrikDB = new ElektrikFaturasiDB();
            elektrikDB.Ay = DateTime.Now.ToString("MMMM");
            elektrikDB.SayactakiOkunanMiktar = (txtSuankiAyinSayactakiKwhTuketimi.Text != "") ? Convert.ToDouble(txtSuankiAyinSayactakiKwhTuketimi.Text) : 0;
            elektrikDB.TekZamanBirimFiyat = (txtTekZamanBirimFiyati.Text != "") ? Convert.ToDouble(txtTekZamanBirimFiyati.Text) : 0;
            elektrikDB.DagitimBedeliBirimFiyat = (txtDagitimBedeliFiyati.Text != "") ? Convert.ToDouble(txtDagitimBedeliFiyati.Text) : 0;

            BusinessLayerResult<ElektrikFaturasiDB> result = ElektMngr.Insert(elektrikDB); // TO:DO tabloda var olan aynı fatura için kayıt eklenemeyecek

            MessageBox.Show(result.messagesObj[0].Message.ToString());
        }

        private void btnTekZamanBirimFiyatiGuncelle_Click(object sender, EventArgs e)
        {
            txtTekZamanBirimFiyati.Enabled = false;
            rdBtnAktifTekZamanBirimFiyati.Checked = false;

            double guncelTekzamanbirimfiyat = (txtTekZamanBirimFiyati.Text != "") ? Convert.ToDouble(txtTekZamanBirimFiyati.Text) : 0;
            string guncellenecekText = "tekzamanbirimfiyat";

            BirimFiyatGuncelle(guncelTekzamanbirimfiyat,guncellenecekText);
        }

        private void btnDagitimBedeliFiyatiGuncelle_Click(object sender, EventArgs e)
        {
            txtDagitimBedeliFiyati.Enabled = false;
            rdBtnDagitimBedeliFiyati.Checked = false;

            double guncelDagitimBedeli = (txtDagitimBedeliFiyati.Text !="") ? Convert.ToDouble(txtDagitimBedeliFiyati.Text) : 0;
            string guncellenecekText = "dagitimbedelibirimfiyat";

            BirimFiyatGuncelle(guncelDagitimBedeli, guncellenecekText);
        }

        private void radBtnSuManuel_CheckedChanged(object sender, EventArgs e)
        {
            if (radBtnSuManuel.Checked == true)
            {
                txtGecenAyinSayactakiMt3Tuketimi.Enabled = true;
                lstBoxGecenAyinMt3Secimi.SelectedItems.Clear();
            }
            else
            {
                txtGecenAyinSayactakiMt3Tuketimi.Enabled = false;
            }
        }

        private void radBtnSuListeden_CheckedChanged(object sender, EventArgs e)
        {
            if (radBtnSuListeden.Checked == true)
            {
                lstBoxGecenAyinMt3Secimi.Enabled = true;
            }
            else
            {
                lstBoxGecenAyinMt3Secimi.Enabled = false;
            }
        }

        private void rdBtnAktifSuBirimFiyati_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnAktifSuBirimFiyati.Checked == true)
            {
                txtSuBirimFiyati.Enabled = true;
            }
            else
            {
                txtSuBirimFiyati.Enabled = false;
            }
        }

        private void btnSuBirimFiyatiGuncelle_Click(object sender, EventArgs e)
        {
            txtSuBirimFiyati.Enabled = false;
            rdBtnAktifSuBirimFiyati.Checked = false;

            if(txtSuBirimFiyati.Text != null && txtSuBirimFiyati.Text != "")
            {
                MessageBox.Show(SuBirimFiyatGuncelle(Convert.ToDouble(txtSuBirimFiyati.Text)));
            }
            else
            {
                MessageBox.Show("Lütfen su birim fiyatı giriniz.");
            }

        }

        private void btnSuFaturasiniHesapla_Click(object sender, EventArgs e)
        {
            SuFaturasi su = new SuFaturasi();
            SuManager suManager = new SuManager();

            su.SuankiAyinOrtakTuketimi = (txtSuankiAyinOrtakTuketimMt3u.Text != "") ? Convert.ToDouble(txtSuankiAyinOrtakTuketimMt3u.Text) : 0;
            su.SuankiAyinSayactakiTuketimi = (txtSuankiAyinSayactakiMt3Tuketimi.Text != "") ? Convert.ToDouble(txtSuankiAyinSayactakiMt3Tuketimi.Text) : 0;

            if(radBtnSuManuel.Checked == true)
            {
                su.GecenAyinSecimDurumu = (int)ManuelListedenSecim.Manuel;
                su.GecenAyinSayactakiTuketimi = (txtGecenAyinSayactakiMt3Tuketimi.Text != "") ? Convert.ToDouble(txtGecenAyinSayactakiMt3Tuketimi.Text) : 0;
            }
            else if (radBtnSuListeden.Checked == true)
            {
                su.GecenAyinSecimDurumu = (int)ManuelListedenSecim.Listeden;

                if(lstBoxGecenAyinMt3Secimi.SelectedItem != null)
                {
                    su.GecenAyinSayactakiSecimiSelect = lstBoxGecenAyinMt3Secimi.SelectedItem.ToString();
                    SuFaturasiDB AyMt3 = suManager.Find(x => x.Ay == su.GecenAyinSayactakiSecimiSelect);

                    su.GecenAyinSayactakiTuketimi = Convert.ToDouble(AyMt3.SayactakiOkunanMiktar);
                }
                else
                {
                    su.GecenAyinSayactakiSecimiSelect = "secimyok";
                    su.GecenAyinSayactakiTuketimi = 0;
                }
            }
            else
            {
                su.GecenAyinSecimDurumu = (int)ManuelListedenSecim.SecimYok;
                su.GecenAyinSayactakiTuketimi = 0;
            }

            //Girilen su faturası bilgileri çekilecek
            su.BirimFiyat = (txtSuBirimFiyati.Text != "") ? Convert.ToDouble(txtSuBirimFiyati.Text) : 0;
            su.BakimBedeli = (txtSuBakimBedeliFiyati.Text != "") ? Convert.ToDouble(txtSuBakimBedeliFiyati.Text) : 0;
            su.KDV = (txtKDVBedeli.Text != "") ? Convert.ToDouble(txtKDVBedeli.Text) : 0;
            su.CTV = (txtCTVBedeli.Text != "") ? Convert.ToDouble(txtCTVBedeli.Text) : 0;

            BusinessLayerResult<SuFaturasi> resultSuFat = suManager.SuFaturasiHesapla(su);

            if(resultSuFat.Obj != null)
            {
                lblToplamOdenecekSuTutari.Text = Math.Round(resultSuFat.Obj.OdenecekTutar, 3).ToString() + " TL";
                lblSuankiAydaHarcadiginizMt3Miktari.Text = Math.Round(resultSuFat.Obj.SuankiAydaHarcananMiktar, 3).ToString() + " Metreküp";

                MessageBox.Show(resultSuFat.messagesObj[0].Message.ToString());
            }
            else
            {
                MessageBox.Show(resultSuFat.messagesObj[0].Message.ToString());
            }
           
        }

        public void GecmisAylarinElektrikFaturasiBilgileriniGetir()
        {
            string GecenAy = DateTime.Now.AddMonths(-1).ToString("MMMM");
            ElektrikManager Em = new ElektrikManager();
            BusinessLayerResult<ElektrikFaturasiDB> EFResult = Em.List();
            List<ElektrikFaturasiDB> GecmisAylarFaturaKhwlari = EFResult.ListObj.ToList();

            foreach (ElektrikFaturasiDB ef in GecmisAylarFaturaKhwlari)
            {
                lstboxGecenAyinKwhSecimi.Items.Add(ef.Ay.ToString()); /* + "-" + ef.SayactakiOkunanMiktar.ToString()*/
            }
        }

        public void GecmisAylarinSuFaturasiBilgileriniGetir()
        {
            string GecenAy = DateTime.Now.AddMonths(-1).ToString("MMMM");
            SuManager Sm = new SuManager();
            BusinessLayerResult<SuFaturasiDB> SuResult = Sm.List();
            List<SuFaturasiDB> GecmisAylarFaturaMt3leri = SuResult.ListObj.ToList();

            foreach (SuFaturasiDB su in GecmisAylarFaturaMt3leri)
            {
                lstBoxGecenAyinMt3Secimi.Items.Add(su.Ay.ToString());
            }

        }

        public ElektrikFaturasiDB GecenAyinBirimveDagitimBilgileriniGetir(bool? guncellemeMi)
        {
            ElektrikManager elekm = new ElektrikManager();
            BusinessLayerResult<ElektrikFaturasiDB> EFResult;

            EFResult = elekm.List();
            
            if(guncellemeMi == false)
            {
                txtTekZamanBirimFiyati.Text = EFResult.ListObj[EFResult.ListObj.Count - 1].TekZamanBirimFiyat.ToString();
                txtDagitimBedeliFiyati.Text = EFResult.ListObj[EFResult.ListObj.Count - 1].DagitimBedeliBirimFiyat.ToString();
            }

            return EFResult.ListObj[EFResult.ListObj.Count - 1];
        }

        public SuFaturasiDB GecenAyinSuBirimFiyatiniGetir(bool? guncellemeMi)
        {
            SuManager suMan = new SuManager();
            BusinessLayerResult<SuFaturasiDB> suResult;

            suResult = suMan.List();

            if(guncellemeMi == false)
            {
                txtSuBirimFiyati.Text = suResult.ListObj[suResult.ListObj.Count - 1].SuBirimFiyat.ToString();
            }

            return suResult.ListObj[suResult.ListObj.Count - 1];

        }

        public void BirimFiyatGuncelle(double guncelbirimfiyat, string guncellenecekText)
        {
            ElektrikManager em = new ElektrikManager();
            ElektrikFaturasiDB efbilgileri = GecenAyinBirimveDagitimBilgileriniGetir(true);
            //elektrikfaturaDB objesini bl'a gönderip, BL'a gönderdiğim objenin id'sine göre db'den Ram'e çekip x.tekbirim = a.tekbirim guncellemesini
            //ram'de yapıp savechanges() ile ram'deki değişiklikleri kayıt(eziyoruz) ediyoruz.

            BusinessLayerResult<ElektrikFaturasiDB> guncelFiyat = new BusinessLayerResult<ElektrikFaturasiDB>();
            guncelFiyat = em.Update(efbilgileri, guncelbirimfiyat, guncellenecekText);

            if (guncelFiyat.Obj != null)
            {
                if(guncellenecekText == "tekzamanbirimfiyat")
                {
                    txtTekZamanBirimFiyati.Text = guncelFiyat.Obj.TekZamanBirimFiyat.ToString();
                }
                else if(guncellenecekText == "dagitimbedelibirimfiyat")
                {
                    txtDagitimBedeliFiyati.Text = guncelFiyat.Obj.DagitimBedeliBirimFiyat.ToString();
                }
            }

            if (guncelFiyat.messagesObj == null)
            {
                MessageBox.Show("BL'da boş nesne");
            }

            MessageBox.Show(guncelFiyat.messagesObj[0].Message.ToString());
        }

        private void groupBox8_Enter(object sender, EventArgs e)
        {

        }

        private void btnSuFaturasiniKaydet_Click(object sender, EventArgs e)
        {
            SuManager suMan = new SuManager();
            SuFaturasiDB suDB = new SuFaturasiDB();

            suDB.Ay = DateTime.Now.ToString("MMMM");
            if(txtSuBirimFiyati.Text != null && txtSuBirimFiyati.Text != "")
            {
                suDB.SuBirimFiyat = Convert.ToDouble(txtSuBirimFiyati.Text);
            }
            else
            {
                MessageBox.Show("Lütfen su birim fiyati giriniz");
            }
            suDB.SayactakiOkunanMiktar = (txtSuankiAyinSayactakiMt3Tuketimi.Text != "") ? Convert.ToDouble(txtSuankiAyinSayactakiMt3Tuketimi.Text) : 0;

            BusinessLayerResult<SuFaturasiDB> suResult = suMan.Insert(suDB);

            MessageBox.Show(suResult.messagesObj[0].Message.ToString());
        }

        public string SuBirimFiyatGuncelle(double suBirimFiyat)
        {
            SuManager su = new SuManager();
            BusinessLayerResult<SuFaturasiDB> resultSu = su.Update(suBirimFiyat);

            return resultSu.messagesObj[0].Message.ToString();
        }
    }
}
