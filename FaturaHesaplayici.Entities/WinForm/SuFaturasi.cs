﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.Entities.WinForm
{
    public class SuFaturasi : FaturaBase
    {
        public string BirimFiyatStatus { get; set; }
        public double BakimBedeli { get; set; }
        public double CTV { get; set; }
    }
}
