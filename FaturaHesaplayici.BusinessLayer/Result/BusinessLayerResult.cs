﻿using FaturaHesaplayici.Entities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.BusinessLayer.Result
{
    public class BusinessLayerResult<T> where T : class
    {
        public T Obj { get; set; }
        public List<MessageObj> messagesObj { get; set; }
        public List<T> ListObj { get; set; }

        public BusinessLayerResult()
        {
            messagesObj = new List<MessageObj>();
        }

        public void MessageAdd(string message, MessageCode code)
        {
            messagesObj.Add(new MessageObj { Code = code, Message = message });
        }

    }
}
