﻿using FaturaHesaplayici.DataAccessLayer.EntityFramework;
using FaturaHesaplayici.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FaturaHesaplayici.BusinessLayer.Abstract
{
    public abstract class ManagerBase<T> : IRepository<T> where T : class
    {
        private Repository<T> DALRepo = new Repository<T>();

        public virtual int Delete(T obj)
        {
            return DALRepo.Delete(obj);
        }

        public virtual T Find(Expression<Func<T, bool>> where)
        {
            return DALRepo.Find(where);
        }

        public virtual int Insert(T obj)
        {
            return DALRepo.Insert(obj);
        }

        public virtual List<T> List()
        {
            return DALRepo.List();
        }

        public virtual List<T> List(Expression<Func<T, bool>> where)
        {
            return DALRepo.List(where);
        }

        public virtual IQueryable<T> ListQueryable()
        {
            return DALRepo.ListQueryable();
        }

        public virtual int Save()
        {
            return DALRepo.Save();
        }

        public virtual int Update(T obj)
        {
            return DALRepo.Update(obj);
        }
    }
}
