﻿namespace FaturaHesaplayici.Entities.Messages
{
    public enum MessageCode
    {
        FaturaBilgileriKayıtlıdır = 1,
        FaturaBilgileriKayıtEdildi = 2,
        FaturaBilgileriEksik=3,
        BosNesne=4,
        KayitliFaturaBilgisiBulunmuyor=5,
        TekZamanBirimFiyatGuncellendi=6,
        TekZamanBirimFiyatGuncellenmeHatasi=7,
        BirimFiyatBulunamadiGuncellemeBasarisiz=8,
        BosReferans=9,
        FaturaBasariylaHesaplandi=10,
        GecenAyinTuketimiBelirtilmedi=11,
        BosAlanlariDoldurunuz=12,
        SuBirimFiyatGuncellendi=13,
        SuBirimFiyatGuncellemesiBasarisiz=14,
        GuncellenecekSuBirimFiyatBulunamadi=15,
        GirilenRakamlariKontrolEdiniz=16,
        GuncellenecekFiyatSifirdanBuyukOlmali=17
    }
}

