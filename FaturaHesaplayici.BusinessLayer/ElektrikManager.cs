﻿using FaturaHesaplayici.BusinessLayer.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaturaHesaplayici.Entities.WinForm;
using FaturaHesaplayici.Entities.DBSets;
using FaturaHesaplayici.BusinessLayer.Result;
using FaturaHesaplayici.Entities.Messages;

namespace FaturaHesaplayici.BusinessLayer
{
    public class ElektrikManager : ManagerBase<ElektrikFaturasiDB>
    {
        //Metot hiding
        public new BusinessLayerResult<ElektrikFaturasiDB> Insert(ElektrikFaturasiDB obj)
        {
            BusinessLayerResult<ElektrikFaturasiDB> EfResult = new BusinessLayerResult<ElektrikFaturasiDB>();

            if(obj != null)
            {
                if(obj.TekZamanBirimFiyat !=0 && obj.DagitimBedeliBirimFiyat != 0 && obj.Ay !="" && obj.SayactakiOkunanMiktar != 0)
                {

                    ElektrikFaturasiDB fatura = Find(x => x.Ay == obj.Ay);

                    if(fatura != null)
                    {
                        EfResult.MessageAdd("Kayıtlı olan fatura tekrardan kayıt edilemez. Kayıt başarısız!", MessageCode.FaturaBilgileriKayıtlıdır);
                    }
                    else
                    {
                        if (base.Insert(obj) == 1)
                        {
                            EfResult.Obj = obj;
                            EfResult.MessageAdd("Fatura bilgileri kayıt edildi.", MessageCode.FaturaBilgileriKayıtEdildi);

                            return EfResult;
                        }
                    }
                   

                }
                else
                {
                    EfResult.Obj = obj;
                    EfResult.MessageAdd("Lütfen elektrik faturası bilgilerini eksiksiz doldurunuz.", MessageCode.FaturaBilgileriEksik);

                    return EfResult;
                }
            }

            EfResult.Obj = obj;
            EfResult.MessageAdd("Boş nesne!", MessageCode.BosNesne);

            return EfResult;
        }

        public new BusinessLayerResult<ElektrikFaturasiDB> List()
        {
            BusinessLayerResult<ElektrikFaturasiDB> EfList = new BusinessLayerResult<ElektrikFaturasiDB>();

            if(base.List().Count == 0)
            {
                EfList.MessageAdd("Kayıtlı fatura bilgisi bulunmuyor.", MessageCode.KayitliFaturaBilgisiBulunmuyor);
            }

            EfList.ListObj = base.List();

            return EfList;
        }

        public BusinessLayerResult<ElektrikFaturasiDB> Update(ElektrikFaturasiDB tekzamanbirimfiyat,double guncellenecekFiyat,string guncellenecekText)
        {
            BusinessLayerResult<ElektrikFaturasiDB> tekzamanGuncel = new BusinessLayerResult<ElektrikFaturasiDB>();

            if(tekzamanbirimfiyat != null)
            {
                ElektrikFaturasiDB efDB = base.Find(x => x.Id == tekzamanbirimfiyat.Id);

                if (efDB != null)
                {
                    if (guncellenecekText == "tekzamanbirimfiyat" && tekzamanbirimfiyat.TekZamanBirimFiyat > 0)
                    {
                        efDB.TekZamanBirimFiyat = guncellenecekFiyat;
                    }
                    else
                    {
                        tekzamanGuncel.MessageAdd("Fiyat güncellenemedi!", MessageCode.GuncellenecekFiyatSifirdanBuyukOlmali);

                        return tekzamanGuncel;
                    }

                    if (guncellenecekText == "dagitimbedelibirimfiyat" && tekzamanbirimfiyat.DagitimBedeliBirimFiyat > 0)
                    {
                        efDB.DagitimBedeliBirimFiyat = guncellenecekFiyat;
                    }
                    else
                    {
                        tekzamanGuncel.MessageAdd("Fiyat güncellenemedi!", MessageCode.GuncellenecekFiyatSifirdanBuyukOlmali);

                        return tekzamanGuncel;
                    }

                    int dbResult = Save();

                    if (dbResult == 1)
                    {
                        tekzamanGuncel.Obj = efDB;
                        tekzamanGuncel.MessageAdd("Fiyat güncellendi.", MessageCode.TekZamanBirimFiyatGuncellendi);

                        return tekzamanGuncel;
                    }
                    else
                    {
                        tekzamanGuncel.MessageAdd("Fiyat güncellenemedi!", MessageCode.TekZamanBirimFiyatGuncellenmeHatasi);

                        return tekzamanGuncel;
                    }
                }
                else
                {
                    tekzamanGuncel.MessageAdd("Böyle bir fiyat kayıtlı değil.Güncelleme yapılamadı.", MessageCode.BirimFiyatBulunamadiGuncellemeBasarisiz);

                    return tekzamanGuncel;
                }
            }

            tekzamanGuncel.MessageAdd("Boş referans hatası.", MessageCode.BosReferans);

            return tekzamanGuncel;
        }

        public BusinessLayerResult<ElektrikFaturasi> FaturaTutariniHesapla(ElektrikFaturasi e)
        {
            BusinessLayerResult<ElektrikFaturasi> eFat = new BusinessLayerResult<ElektrikFaturasi>();

            if(e != null)
            {
                if(e.DagitimBedeliBirimFiyat != 0 && e.ElektveHvgTukVerBedeli != 0 && e.EnerjiFonuBedeli != 0 && e.KDV != 0 && e.SuankiAyinOrtakTuketimi != 0 && e.SuankiAyinSayactakiTuketimi != 0 && e.TekZamanBirimFiyat != 0 && e.TrtPayiBedeli != 0)
                {
                    if(e.GecenAyinSayactakiSecimiSelect != "" || e.GecenAyinSayactakiTuketimi != 0)
                    {
                        e.SuankiAydaHarcananMiktar = e.SuankiAyinSayactakiTuketimi - e.GecenAyinSayactakiTuketimi;

                        e.ToplamOdenecekTutar += e.SuankiAydaHarcananMiktar * e.TekZamanBirimFiyat;
                        e.ToplamOdenecekTutar += e.SuankiAydaHarcananMiktar * e.DagitimBedeliBirimFiyat;

                        e.ToplamOdenecekTutar += VergiveFonlarHesapla(e);

                        if(e.ToplamOdenecekTutar < 0)
                        {
                            eFat.MessageAdd("Lütfen girdiğiniz bilgileri kontrol ediniz.",MessageCode.GirilenRakamlariKontrolEdiniz);
                        }
                        else
                        {
                            eFat.Obj = e;
                            eFat.MessageAdd("Fatura başarıyla hesaplandı.", MessageCode.FaturaBasariylaHesaplandi);
                        }
                        

                        return eFat;
                    }

                    eFat.MessageAdd("Lütfen geçen ayın tüketimini belirtiniz.", MessageCode.GecenAyinTuketimiBelirtilmedi);

                    return eFat;
                }

                eFat.MessageAdd("Lütfen boş alanları doldurunuz.", MessageCode.BosAlanlariDoldurunuz);

                return eFat;
            }

            eFat.MessageAdd("Lütfen boş alanları doldurunuz.", MessageCode.BosAlanlariDoldurunuz);

            return eFat;
        }

        public double VergiveFonlarHesapla(ElektrikFaturasi e)
        {
            return EnerjiFonuBedeliniHesapla(e) + TrtPayiBedeliniHesapla(e) + ElektveHvgTukVerBedeliBedeliniHesapla(e) + KDVBedeliniHesapla(e);
        }

        public double EnerjiFonuBedeliniHesapla(ElektrikFaturasi e)
        {
            double EnerjiFonuBedeli = 0, birkwhadusenTL=0;

            birkwhadusenTL = e.EnerjiFonuBedeli / e.SuankiAyinOrtakTuketimi;
            EnerjiFonuBedeli = birkwhadusenTL * e.SuankiAydaHarcananMiktar;

            return EnerjiFonuBedeli;
        }

        public double TrtPayiBedeliniHesapla(ElektrikFaturasi e)
        {
            double TrtPayiBedeli = 0, birkwhadusenTL=0;

            birkwhadusenTL = e.TrtPayiBedeli / e.SuankiAyinOrtakTuketimi;
            TrtPayiBedeli = birkwhadusenTL * e.SuankiAydaHarcananMiktar;

            return TrtPayiBedeli;
        }

        public double ElektveHvgTukVerBedeliBedeliniHesapla(ElektrikFaturasi e)
        {
            double ElektveHvgTukVerBedeli = 0, birkwhadusenTL=0;

            birkwhadusenTL = e.ElektveHvgTukVerBedeli / e.SuankiAyinOrtakTuketimi;
            ElektveHvgTukVerBedeli = birkwhadusenTL * e.SuankiAydaHarcananMiktar;

            return ElektveHvgTukVerBedeli;
        }

        public double KDVBedeliniHesapla(ElektrikFaturasi e)
        {
            double KDV = 0, birkwhadusenTL=0;

            birkwhadusenTL = e.KDV / e.SuankiAyinOrtakTuketimi;
            KDV = birkwhadusenTL * e.SuankiAydaHarcananMiktar;         

            return KDV;
        }
    }
}
